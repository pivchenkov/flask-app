import os
import requests

TOKEN = os.getenv("TOKEN")
URL = "https://api.github.com/repos/boto/boto3/pulls"
HEADERS = {'Authorization': f'Bearer {TOKEN}'}
PARAMS = dict()
PARAMS["per_page"] = 100


def check_labels(labels_list, status):
    if len(labels_list["labels"]) > 0:
        for item in labels_list["labels"]:
            if item["name"] == status:
                return True

    return False


def make_request(state_params):
    local_params = PARAMS
    local_params["state"] = state_params
    try:
        response = requests.get(URL, headers=HEADERS, params=local_params)
    except Exception as e:
        print("Error getting data:", e)
        return False
    else:
        if response.status_code == 200:
            return response
        else:
            print("Response status code:", response.status_code)
            return False


def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """

    result = []
    if state == "open":
        response = make_request("open")
        if response:
            for item in response.json():
                if item["state"] == "open" and len(item["labels"]) == 0:
                    pull = {}
                    pull["num"] = item["number"]
                    pull["title"] = item["title"]
                    pull["link"] = item["html_url"]
                    result.append(pull)
            return result
        else:
            print("Response func returned False")
            return False

    if state == "closed":
        response = make_request("closed")
        if response:
            for item in response.json():
                if item["state"] == "closed":
                    pull = {}
                    pull["num"] = item["number"]
                    pull["title"] = item["title"]
                    pull["link"] = item["html_url"]
                    result.append(pull)
            return result
        else:
            print("Response func returned False")
            return False

    if state == "bug":
        response = make_request("open")
        if response:
            for item in response.json():
                if item["state"] == "open" and check_labels(item, "bug"):
                    pull = {}
                    pull["num"] = item["number"]
                    pull["title"] = item["title"]
                    pull["link"] = item["html_url"]
                    result.append(pull)
            return result
        else:
            print("Response func returned False")
            return False

    if state == "needs-review":
        response = make_request("open")
        if response:
            for item in response.json():
                if item["state"] == "open" and check_labels(item, "needs-review"):
                    pull = {}
                    pull["num"] = item["number"]
                    pull["title"] = item["title"]
                    pull["link"] = item["html_url"]
                    result.append(pull)
            return result
    else:
        print("Response func returned False")
        return False
